var crypto = require('crypto');
var bufferEqual = require('./buffer-equal');


/**
 * @constructor
 */
var AccessToken = function () {
  this.user_id = null;

  this.created_at = Date.now();
  this.expiration = 0;

  this.decrypted_ = true;
};


AccessToken.VERSION = 1;


/**
 * @param {string} key A server-side key.
 */
AccessToken.prototype.serialize = function (key) {
  var key_buf = new Buffer(key);
  var cipher = crypto.createCipher('aes192', key_buf);

  var data = this.serializeData_();
  var data_buf = cipher.update(data, 'utf8', 'buffer');
  data_buf = Buffer.concat([ data_buf, cipher.final('buffer') ]);

  var version_buf = AccessToken.getVersionBuffer();
  var auth_buf = this.getAuthBuffer();

  var token_buf = new Buffer(version_buf.length + auth_buf.length + data_buf.length);
  var i = 0;
  i += version_buf.copy(token_buf, i);
  i += auth_buf.copy(token_buf, i);
  i += data_buf.copy(token_buf, i);

  return token_buf.toString('hex');
};


AccessToken.prototype.serializeData_ = function () {
  var data_parts = [ this.created_at, this.expiration ];
  return data_parts.join(':');
};


AccessToken.prototype.readData_ = function (data) {
  var data_parts = data.split(':');
  if (data_parts.length !== 2) {
    return false;
  }

  this.created_at = Number(data_parts[0]) || 0;
  this.expiration = Number(data_parts[1]) || 0;
  return true;
};


AccessToken.prototype.resetData_ = function () {
  this.created_at = 0;
  this.expiration = 0;
};


AccessToken.prototype.isDecrypted = function () {
  return this.decrypted_;
};


AccessToken.prototype.setEncryptedData = function (data_buf) {
  this.data_buf_ = data_buf;

  this.resetData_();
  this.decrypted_ = false;
};


AccessToken.prototype.decryptWithKey = function (key) {
  if (!this.data_buf_) {
    return false;
  }

  var data;
  var key_buf = new Buffer(key);
  var decipher = crypto.createDecipher('aes192', key_buf);
  try {
    data = decipher.update(this.data_buf_, 'buffer', 'utf8');
    data += decipher.final('utf8');
  } catch (err) {
    return false;
  }

  this.decrypted_ = this.readData_(data);
  return this.decrypted_;
};


AccessToken.prototype.getAuthBuffer = function () {
  var user_id = this.user_id || '';

  var auth_buf = new Buffer(1 + user_id.length);
  auth_buf[0] = user_id.length & 0xFF;
  auth_buf.write(user_id, 1);

  return auth_buf;
};



AccessToken.getVersionBuffer = function () {
  var version_buf = new Buffer('v___');
  version_buf[1] = AccessToken.VERSION & 0xFF;
  version_buf[2] = (AccessToken.VERSION >> 8) & 0xFF;
  version_buf[3] = 0x00;

  return version_buf;
};


AccessToken.validate = function (access_token) {
  var version_buf = AccessToken.getVersionBuffer();
  var length = version_buf.length;

  var token_buf = new Buffer(access_token, 'hex');
  return (bufferEqual(version_buf, token_buf.slice(0, length)));
};


AccessToken.parse = function (access_token) {
  if (!AccessToken.validate(access_token)) {
    return null;
  }

  var token_buf = new Buffer(access_token, 'hex');
  var version_buf = AccessToken.getVersionBuffer();

  var auth_offset = version_buf.length;
  var user_id_length = token_buf[auth_offset];
  var data_offset = auth_offset + 1 + user_id_length;

  var token = new AccessToken();
  token.setEncryptedData(token_buf.slice(data_offset));

  var user_id_buf = token_buf.slice(auth_offset + 1, data_offset);
  token.user_id = user_id_buf.toString('utf8') || null;

  return token;
};


AccessToken.parseWithKey = function (access_token, key) {
  var token = AccessToken.parse(access_token);
  if (!token) {
    return null;
  }

  token.decryptWithKey(key);

  return token;
};


module.exports = AccessToken;
