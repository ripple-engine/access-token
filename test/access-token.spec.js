var crypto = require('crypto');

var AccessToken = require('../src/access-token');


describe('AccessToken', function () {
  describe('generation', function () {
    it('should generate a unique token given the same key and different data',
        function () {
      var key = crypto.pseudoRandomBytes(16);

      var token1 = new AccessToken();
      var token2 = new AccessToken();
      token1.expiration = 3000;
      token2.expiration = 1000;

      var access_token1 = token1.serialize(key);
      var access_token2 = token2.serialize(key);
      expect(access_token1).to.not.be(access_token2);
    });


    it('should generate the same token given the same key and data',
        function () {
      var key = crypto.pseudoRandomBytes(16);

      var token1 = new AccessToken();
      var token2 = new AccessToken();

      var access_token1 = token1.serialize(key);
      var access_token2 = token2.serialize(key);
      expect(access_token1).to.be(access_token2);
    });
  });


  describe('parsing', function () {
    it('should parse an unauthenticated token given a key', function () {
      var input_token = new AccessToken();
      input_token.created_at = Date.now() - 10000000;
      input_token.expiration = 3600000;

      var key = crypto.pseudoRandomBytes(16);
      var access_token = input_token.serialize(key);

      var output_token = AccessToken.parseWithKey(access_token, key);
      expect(output_token.user_id).to.be(null);
      expect(output_token.created_at).to.be(input_token.created_at);
      expect(output_token.expiration).to.be(input_token.expiration);
      expect(output_token.isDecrypted()).to.be(true);
    });


    it('should parse an authenticated token given a key', function () {
      var input_token = new AccessToken();
      input_token.user_id = 'abcd1234';
      input_token.created_at = Date.now() - 10000000;
      input_token.expiration = 3600000;

      var key = crypto.pseudoRandomBytes(16);
      var access_token = input_token.serialize(key);

      var output_token = AccessToken.parseWithKey(access_token, key);
      expect(output_token.user_id).to.be(input_token.user_id);
      expect(output_token.created_at).to.be(input_token.created_at);
      expect(output_token.expiration).to.be(input_token.expiration);
      expect(output_token.isDecrypted()).to.be(true);
    });


    it('should parially parse an unauthenticated token given a key',
        function () {
      var input_token = new AccessToken();
      input_token.created_at = Date.now() - 10000000;
      input_token.expiration = 3600000;

      var key = crypto.pseudoRandomBytes(16);
      var access_token = input_token.serialize(key);

      var output_token = AccessToken.parse(access_token);
      expect(output_token.user_id).to.be(null);
      expect(output_token.created_at).to.be(0);
      expect(output_token.expiration).to.be(0);
      expect(output_token.isDecrypted()).to.be(false);
    });


    it('should parially parse an authenticated token given a key',
        function () {
      var input_token = new AccessToken();
      input_token.user_id = 'abcd1234';
      input_token.created_at = Date.now() - 10000000;
      input_token.expiration = 3600000;

      var key = crypto.pseudoRandomBytes(16);
      var access_token = input_token.serialize(key);

      var output_token = AccessToken.parse(access_token);
      expect(output_token.user_id).to.be(input_token.user_id);
      expect(output_token.created_at).to.be(0);
      expect(output_token.expiration).to.be(0);
      expect(output_token.isDecrypted()).to.be(false);
    });
  });


  describe('decryption', function () {
    it('should decrypt its data with a key', function () {
      var input_token = new AccessToken();
      input_token.created_at = Date.now() - 10000000;
      input_token.expiration = 3600000;

      var key = crypto.pseudoRandomBytes(16);
      var access_token = input_token.serialize(key);
      var output_token = AccessToken.parse(access_token);

      var success = output_token.decryptWithKey(key);
      expect(output_token.created_at).to.be(input_token.created_at);
      expect(output_token.expiration).to.be(input_token.expiration);
      expect(output_token.isDecrypted()).to.be(true);
      expect(success).to.be(true);
    });


    it('should return false on a wrong encryption key', function () {
      var input_token = new AccessToken();
      input_token.created_at = Date.now() - 10000000;
      input_token.expiration = 3600000;

      var key = crypto.pseudoRandomBytes(16);
      key[0] = 1;
      var access_token = input_token.serialize(key);
      var output_token = AccessToken.parse(access_token);

      key[0] = 2;
      var success = output_token.decryptWithKey(key);
      expect(output_token.created_at).to.not.be(input_token.created_at);
      expect(output_token.expiration).to.not.be(input_token.expiration);
      expect(output_token.isDecrypted()).to.be(false);
      expect(success).to.be(false);
    });
  });
});
